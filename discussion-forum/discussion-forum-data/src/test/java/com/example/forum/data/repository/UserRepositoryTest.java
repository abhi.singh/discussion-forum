package com.example.forum.data.repository;

import static org.junit.Assert.*;
import org.junit.Test;

import com.example.forum.data.entity.User;

public class UserRepositoryTest extends AbstractMongoDBTest {

	@Test
	public void testCreateUser() throws Exception {
		User u = new User("test", "Test 1", "Test 2", "t@t.com", "123456");
		u = userRepository.save(u);
		assertNotNull(u.getId());
		System.out.println("user id " + u.getId());
		assertNotNull(userRepository.findOne(u.getId()));
	}

	@Test
	public void testUpdateUser() throws Exception {
		User u = new User("test", "Test 1", "Test 2", "t@t.com", "123456");
		u = userRepository.save(u);
		assertNotNull(u.getId());
		System.out.println("user id " + u.getId());
		u.setUsername("admin");
		userRepository.save(u);

		assertEquals("admin", userRepository.findOne(u.getId()).getUsername());
	}

	@Test
	public void testDeleteUser() throws Exception {
		User u = new User("test", "Test 1", "Test 2", "t@t.com", "123456");
		u = userRepository.save(u);
		assertNotNull(u.getId());
		System.out.println("user id " + u.getId());
		userRepository.delete(u);
		assertNull(userRepository.findOne(u.getId()));
	}

}
