package com.example.forum.data.repository;

import org.junit.AfterClass;
import org.junit.BeforeClass;

import com.example.forum.data.repository.helper.Repositories;

import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodProcess;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;

public abstract class AbstractMongoDBTest {

	/**
	 * please store Starter or RuntimeConfig in a static final field if you want
	 * to use artifact store caching (or else disable caching)
	 */
	private static final MongodStarter starter = MongodStarter
			.getDefaultInstance();

	static private MongodExecutable _mongodExe;
	static private MongodProcess _mongod;
	static private int port = 27017;

	static DiscussionThreadRepository dtRepository;
	static UserRepository userRepository;
	static DiscussionThreadEntryRepository dtEntryRepository;

	@BeforeClass
	public static void setUp() throws Exception {
		_mongodExe = starter.prepare(new MongodConfigBuilder()
				.version(Version.Main.PRODUCTION)
				.net(new Net(port, Network.localhostIsIPv6())).build());
		_mongod = _mongodExe.start();

		Thread.sleep(1000);

		userRepository = Repositories.getUserDatastore();
		dtRepository = Repositories.getDiscussionThreadDatastore();
		userRepository = Repositories.getUserDatastore();
		dtEntryRepository = Repositories.getDiscussionThreadEntryDatastore();
	}

	@AfterClass
	public static void tearDown() throws Exception {

		_mongod.stop();
		_mongodExe.stop();
	}

}