package com.example.forum.data.repository;

import java.math.BigInteger;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.forum.data.entity.User;

@Repository
public interface UserRepository extends
		CrudRepository<User, BigInteger> {

}
