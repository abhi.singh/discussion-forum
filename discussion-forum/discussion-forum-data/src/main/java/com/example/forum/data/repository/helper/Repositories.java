package com.example.forum.data.repository.helper;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.example.forum.data.ApplicationConfig;
import com.example.forum.data.repository.DiscussionThreadEntryRepository;
import com.example.forum.data.repository.DiscussionThreadRepository;
import com.example.forum.data.repository.UserRepository;

public class Repositories {

	private final static ApplicationContext ctx;

	static {
		ctx = new AnnotationConfigApplicationContext(ApplicationConfig.class);
	}

	public static UserRepository getUserDatastore() {
		return ctx.getBean(UserRepository.class);
	}

	public static DiscussionThreadRepository getDiscussionThreadDatastore() {
		return ctx.getBean(DiscussionThreadRepository.class);
	}

	public static DiscussionThreadEntryRepository getDiscussionThreadEntryDatastore() {
		return ctx.getBean(DiscussionThreadEntryRepository.class);
	}

}
