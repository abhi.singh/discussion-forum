package com.example.forum.data.repository;

import java.math.BigInteger;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.example.forum.data.entity.DiscussionThread;

public interface DiscussionThreadRepository extends
		PagingAndSortingRepository<DiscussionThread, BigInteger> {

}
