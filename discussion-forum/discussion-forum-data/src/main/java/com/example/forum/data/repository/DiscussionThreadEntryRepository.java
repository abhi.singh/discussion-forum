package com.example.forum.data.repository;

import java.math.BigInteger;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.example.forum.data.entity.DiscussionThreadEntry;

public interface DiscussionThreadEntryRepository extends
		PagingAndSortingRepository<DiscussionThreadEntry, BigInteger> {

}
