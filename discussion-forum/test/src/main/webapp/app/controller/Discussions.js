Ext.define('DF.controller.Discussions', {
	extend : 'Ext.app.Controller',

	stores : [ 'Discussions' ],
	models : [ 'Discussion' ],
	views : [ 'discussion.List' ],

	init : function() {
		console.log("init discussion controller");
	}
});