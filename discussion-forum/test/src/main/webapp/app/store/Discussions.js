Ext.define('DF.store.Discussions', {
	extend : 'Ext.data.Store',
	model : 'DF.model.Discussion',
	autoLoad : true,

	proxy : {
		type : 'ajax',
		url : '/data/discussions.json'
	}
});