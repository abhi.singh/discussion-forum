Ext.define('DF.model.Discussion', {
	extend : 'Ext.data.Model',
	fields : [ {
		name : 'id',
		type : 'int'
	}, {
		name : 'title',
		type : 'string'
	}, {
		name : 'createdBy',
		type : 'string'
	}, {
		name : 'date',
		type : 'date'
	}, {
		name : 'threads',
		type : 'string'
	} ]
});