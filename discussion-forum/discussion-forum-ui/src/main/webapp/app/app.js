Ext.Loader.setConfig({
	enabled : true,
	disableCache : true
});

Ext.application({
	name : 'DF',
	appFolder : "app",
	autoCreateViewport : true,

	models : [ 'Discussion' ],
	stores : [ 'Discussions' ],
	controllers : [ 'Discussions' ],

// launch : function() {
// // alert("hello");
// }
});
