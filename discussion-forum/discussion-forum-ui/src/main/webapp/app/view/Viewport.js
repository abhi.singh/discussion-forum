Ext.define('DF.view.Viewport', {
	extend : 'Ext.container.Viewport',
	requires : [ 'Ext.container.Viewport'],
	layout : 'border',
	title : 'Home',

	items : [ {
		region : 'north',
		xtype : 'panel',
		height : 100,
		margin : '0 20 0 20',
		html : '<h1>Discussion Board</h1>'
	}, {
//		title : 'Center Region',
		region : 'center',
		xtype : 'discussion-grid',
		margin : '0 10 0 20',
		flex : 3
	}, {
		title : 'East Region',
		region : 'east',
		xtype : 'panel',
		margin : '0 20 0 20',
		flex : 1
	}, {
		title : 'South Region is resizable',
		region : 'south',
		xtype : 'panel',
		margin : '0 20 2 20',
		height : 100,
	} ],
	renderTo : Ext.getBody()
});