Ext.define('DF.view.discussion.List', {
	extend : 'Ext.grid.Panel',
	requires : [ 'Ext.grid.column.Action' ],

	xtype : 'discussion-grid',
	store : 'Discussions',
	title : 'Discussions',

	viewConfig : {
		stripeRows : true,
		enableTextSelection : true
	},

	initComponent : function() {
		this.columns = [ {
			text : 'Title',
			flex : 3,
			sortable : false,
			dataIndex : 'title'
		}, {
			text : 'Created By',
			flex : 1,
			sortable : true,
			dataIndex : 'createdBy'
		}, {
			text : 'Date',
			flex : 1,
			sortable : true,
			renderer : Ext.util.Format.dateRenderer('m/d/Y'),
			dataIndex : 'date'
		}, ];

		this.callParent();
	}
});